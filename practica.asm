
; ********************************RECURSOS ******************************
;
; https://www.csie.ntu.edu.tw/~acpang/course/asm_2004/slides/chapt_09.pdf
; http://ict.udlap.mx/people/oleg/docencia/ASSEMBLER/asm_interrup_21.html
; https://www.cs.buap.mx/~mgonzalez/asm_mododir2.pdf
; https://elcodigoascii.com.ar/
; https://moisesrbb.tripod.com/unidad5.htm#u541
; http://www.nachocabanes.com/tutors/asmperif.htm

; http://atc2.aut.uah.es/~avicente/asignaturas/ec/pdf/ec_t4.pdf
; http://arantxa.ii.uam.es/~gdrivera/labetcii/practica0/alumno.htm
; http://miensamblador.blogspot.com/2013/05/captura-de-un-enter.html
; http://www.gabrielececchetti.it/Teaching/CalcolatoriElettronici/Docs/i8086_and_DOS_interrupts.pdf
; ***********************************************************************




;#-----------------------------------------------#
;#                 Krlos López                   #
;#                  201313894                    #
;#             ♡ Debian + Gnome ♡                #
;#-----------------------------------------------#



; ***
; IMPRIMIR CADENAS Y EL VECTOR PARA FORMAR EL TABLERO
; ***
imprimir macro cadena 
  mov ax,@data  ; carga el segmento de datos para obtener los datos
  mov ds,ax     ; en el segmento de datos
  mov ah,09h    ; 
  mov dx,offset cadena  ; Direccion del texto a imprimir
  int 21h       ; interrupcion del DOS 21h 
endm




; ***
; RECORRO EL VECTOR PARA PODER IMPRIMIR EL TABLERO
; ***
imprimirTablero macro vector
  LOCAL Mientras_, FinMientras_, ImprimirSalto
  xor si,si   ; limpia el registro
  mov si, 1   ; inicializa el contador si=1
  xor di,di   ; limpia el registro

    Mientras_:

      cmp si,82   ; if (resta (82-si) donde si++)
      
        je FinMientras_				  ; while(si<=82){}

          mov al, vector[si]    ; mueve el valor al registro 'al'
          mov auxiliarTable, al 
          imprimir auxiliarTable

          cmp di,8              ; if(di == 8){ Imprimir salto}
            je ImprimirSalto
          
          mov auxiliarTable,32  ; else { " " }
            imprimir auxiliarTable  

          inc di        ; di++}
          inc si   			; si++}

        jmp Mientras_


      ImprimirSalto:
        xor di,di 			; di = 0
        imprimir salto  ; print("/n")
        inc si  			  ; si++
        jmp Mientras_

    FinMientras_:
endm




; ***
; CAPTURA LA OPCION TOMADA DE LA TERMINAL 
; ***
capturarCaracter macro
  mov ah, 01h ;mueve el primer caracter de entrada a la posicion del registro 'ah'
  int 21h
endm








; ***
; SELECCION DEL MENU DE LA TERMINAL
; ***
; ***
opcionJugar macro seleccion 
  mov seleccion[0], 31h 
  je jugar
endm 
opcionImprimirReporte macro seleccion 
  mov seleccion[0], 33h
  je salir
  jmp inicio  ; brinco si o si a inicio del juego
endm 

selectMenu macro opcion 
  capturarCaracter

  cmp al,31h  ; if(al == 1) {jugar}
    opcionJugar opcion
  
  cmp al, 32h ; else if(al == 2) {imprimir reporte}
    opcionImprimirReporte opcion
endm
 
; ***
; IMPRIMIR REPORTE
; ***
imprimirReporte macro 
  capturarCaracter
  cmp al, 33h   ; if(33h == 3) { imprimir reporte }
    mov option[0], 33h
    je reporte  ; brinco al macro reporte
    imprimir msgReporte 
    je jugar
endm 






; ***
; CAPTURO EL TEXTO DEL NOMBRE DEL JUGADOR
; ***
capturarNombreDelJugador macro buffer
  LOCAL mientrasNombre, finEntrada
  xor si, si  ; limpio el registro
  
  mientrasNombre:     ; while (recorro la cadena ingresada)
    cicloPedirReporteOtros finEntrada, buffer
  jmp mientrasNombre

  finEntrada:
    limpiarPantallaVector buffer
endm

cicloPedirReporteOtros macro finEntrada, buffer 
  capturarCaracter
    cmp al, 33h ; if (caracter == 3) {generar reporte}
      je reporte

    cmp al, 0dh ; captura el enter
    je finEntrada  ; brinco a la siguiente opcion fuera del ciclo
    mov buffer[si], al
    inc si      ; incremento
endm 

limpiarPantallaVector macro buffer 
  mov al, 00h ; limpia la pantalla 
  mov buffer[si], al
endm






; ***
; GUARDA EL VALOR 'NOMBRE' INGRESADO DEL JUGADOR
; ***
guardaNombreDelJugadorEnVector macro nickName
    mov ah, 3fh ; lee la informacion
    mov bx, 00  ; limpia el registro
    mov cx, 100 ; los 100 espacios reservados
    mov dx, offset[nickName] ;busca lo que se encuentra en el registro de la variable
    int 21h
endm





iniciarJuego macro
   ;-------------Captura los nombres de jugador
  imprimir msgJugadorB
  guardaNombreDelJugadorEnVector nick1
  imprimir msgJugadorN
  guardaNombreDelJugadorEnVector nick2

  imprimir salto
  imprimirTablero tablero


  ; turno BLANCAS
  turno_blanco:
  ;genera reporte automaticamente
    crearArchivo ruta 
    escribirArchivo etiqueta1
  ; imprime demas etiquetas
  imprimir nick1
  imprimir turnoB
  imprimir puntosB
  imprimir movimientoB
  imprimir salto
  capturarNombreDelJugador str_instr
  imprimirTablero tablero

  ; turno NEGRAS
  turno_negra:
  ;genera reporte automaticamente
    crearArchivo ruta 
    escribirArchivo etiqueta1
  ; imprime demas etiquetas
  imprimir nick2
  imprimir turnoN
  imprimir puntosN
  imprimir movimientoN
  imprimir salto
  capturarNombreDelJugador str_instr
  imprimirTablero tablero


  jmp turno_blanco
endm








; https://hopelchen.tecnm.mx/principal/sylabus/fpdb/recursos/r89584.PDF

crearArchivo macro nombre
     ; Para crear archivos21h interrumpido3función ch
    mov cx, 0       ; Atributos de archivo
    lea dx, nombre  ; mueve los datos al registro indicado, lea == offset 
    mov ah, 3ch     ; crea un archivo
    int 21h

        ; Utilice los parámetros de exportación para determinar si tiene éxito, si tiene éxito, guarde el identificador
    jc stop    ;CF=1Entonces detente
    mov handle, ax; guardar identificador de archivo
endm

escribirArchivo macro texto 
    mov bx, handle  ; Identificador 'datos para escribir en el' archivo
    mov cx, offset handle - offset texto  ; tipo resta de la cantidad que reserva - los caracteres a escribir
    lea dx, texto  ; mueve los datos al resgistro
    mov ah, 40h     ; escribe en un archivo
    int 21h

    ; Utilice los parámetros de salida para juzgar si tiene éxito o salga si falla
    jc stop

    mov bx, handle 
    mov ah, 3eh     ; cierra el archivo
    int 21h
endm 






.model small
.stack


.data
  ruta db 'reporte.html',0
  txt db 550 dup('$')
  txtc db 'archivo creado', '$'
  msgReporteError db 0ah, 0dh, "!... Reporte NO Generado ...! ", "$"

  ;************************************************* etiquetas ********************************************************
    etiqueta1 db '<table class=default border=#000000 ><tr>', 10,13
              db '<tr><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td> </tr>', 10, 13
              db '<tr><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td></tr>',  10, 13
              db '<tr><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td><td>   B   </td><td>       </td></tr>',  10, 13
              db '<tr><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>',  10, 13
              db '<tr><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>',  10, 13
              db '<tr><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td>',       10, 13
              db '</tr><tr><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><tr>',  10, 13
              db '<tr><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td><td>       </td><td>   N   </td>',       10, 13
              db '</tr></table>',  10, 13
  handle dw ?    ; Guardar asa
  ;********************************************************************************************************************

  ;********************variables de interrupcion y declaracion**************************************
  encabezado1 db 0ah, 0dh, "   * UNIVERSIDAD DE SAN CARLOS DE GUATEMALA            ", "$"
  encabezado2 db 0ah, 0dh, "   * FACULTAD DE INGENIERIA                            ", "$"
  encabezado3 db 0ah, 0dh, "   * ESCUELA DE CIENCIAS Y SISTEMAS                    ", "$"
  encabezado4 db 0ah, 0dh, "   * ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1 B  ", "$"
  encabezado5 db 0ah, 0dh, "   * PRIMER SEMESTRE 2021                              ", "$"
  encabezado6 db 0ah, 0dh, "   * NOMBRE: Carlos Arnoldo Lopez Coroy                ", "$"
  encabezado7 db 0ah, 0dh, "   * CARNET: 201313894                                 ", "$"
  encabezado8 db 0ah, 0dh, "   * DOLOROSA Primera Practica Assembler               ", "$"
  encabezado9 db 0ah, 0dh, "                                                       ", "$"
  encabezado10 db 0ah, 0dh, "    1) Jugar   2) Salir  3) Genera Reporte            ", "$"

  ;blancas
  msgJugadorB db 10,13,7, 'Ingresa tu nick turno piezas BLANCAS: ','$'
  turnoB      db 0ah, 0dh, "Turno Blancas: ACTIVO", "$"
  nick1       db 100 dup(' '), '$'
  puntosB     db 0ah, 0dh, 'Punteo actual:', '$'
  movimientoB db 0ah, 0dh, 'Realizar movimiento:  ', '$'
  ;negras
  msgJugadorN db 10,13,7, 'Ingresa tu nick turno piezas NEGRAS: ','$'
  turnoN      db 0ah, 0dh, "  Turno Negras: ACTIVO", "$"
  nick2       db 100 dup(' '), '$'
  puntosN     db 0ah, 0dh, 'Punteo actual:', '$'
  movimientoN db 0ah, 0dh, 'Realizar movimiento:  ', '$'
  

  ; formar el tablero
  tablero db 82 dup('$'), '$'
  auxiliarTable db 0, '$'
  
  ; mensajes varios
  salto db 0ah, 0dh, "$"
  str_instr db 4 dup('$')
  opcionActual db ' ', '$'
  turno db 0b
  msgReporte db 0ah, 0dh, "!... Reporte Generado ...! ", "$"
  textoOpcion db 0ah, 0dh, "Menu de Opciones: ", "$" ;condiciones iniciales para el juego






.code

main proc
inicio:
  
    ;-------------Mostrar encabezado
    imprimir encabezado1
    imprimir encabezado2
    imprimir encabezado3
    imprimir encabezado4
    imprimir encabezado5
    imprimir encabezado6
    imprimir encabezado7
    imprimir encabezado8
    imprimir encabezado9
    imprimir encabezado10
    imprimir salto
    imprimir salto
    imprimir salto
    ;-------------dibujo el tablero
    mov tablero[1],32
    mov tablero[2],97
    mov tablero[3],98
    mov tablero[4],99
    mov tablero[5],100
    mov tablero[6],101
    mov tablero[7],102
    mov tablero[8],103
    mov tablero[9],104
    mov tablero[10],49 ;1
    mov tablero[11],66 ;B
    mov tablero[12],32
    mov tablero[13],66 ;B
    mov tablero[14],32
    mov tablero[15],66 ;B
    mov tablero[16],32
    mov tablero[17],66 ;B
    mov tablero[18],32
    mov tablero[13],66 ;B
    mov tablero[14],32
    mov tablero[15],66 ;B
    mov tablero[16],32
    mov tablero[17],66 ;B
    mov tablero[18],32
    mov tablero[19],50 ;2
    mov tablero[20],32
    mov tablero[21],66 ;B
    mov tablero[22],32
    mov tablero[23],66 ;B
    mov tablero[24],32
    mov tablero[25],66 ;B
    mov tablero[26],32
    mov tablero[27],66 ;B
    mov tablero[28],51 ;3
    mov tablero[29],66
    mov tablero[30],32
    mov tablero[31],66 ;B
    mov tablero[32],32
    mov tablero[33],66 ;B
    mov tablero[34],32
    mov tablero[35],66 ;B
    mov tablero[36],32
    mov tablero[37],52 ;4
    mov tablero[38],32
    mov tablero[39],32
    mov tablero[40],32
    mov tablero[41],32
    mov tablero[42],32
    mov tablero[43],32
    mov tablero[44],32
    mov tablero[45],32
    mov tablero[46],53 ;5
    mov tablero[47],32
    mov tablero[48],32
    mov tablero[49],32
    mov tablero[50],32
    mov tablero[51],32
    mov tablero[52],32
    mov tablero[53],32
    mov tablero[54],32
    mov tablero[55],54 ;6
    mov tablero[56],32
    mov tablero[57],78 ;N
    mov tablero[58],32
    mov tablero[59],78 ;N
    mov tablero[60],32
    mov tablero[61],78 ;N
    mov tablero[62],32
    mov tablero[63],78 ;N
    mov tablero[64],55 ;7
    mov tablero[65],78 ;N
    mov tablero[66],32
    mov tablero[67],78 ;N
    mov tablero[68],32
    mov tablero[69],78 ;N
    mov tablero[70],32
    mov tablero[71],78 ;N
    mov tablero[72],32
    mov tablero[73],56 ;8
    mov tablero[74],32
    mov tablero[75],78 ;N
    mov tablero[76],32
    mov tablero[77],78 ;N
    mov tablero[78],32
    mov tablero[79],78 ;N
    mov tablero[80],32
    mov tablero[81],78 ;N

    imprimir textoOpcion
    selectMenu opcionActual
    imprimir salto
    imprimir salto

  jugar:
    iniciarJuego
    jmp inicio
  cargar:
    jmp inicio
  reporte:
    ;crearArchivo ruta 
    ;escribirArchivo etiqueta1
  salir:
    mov ah, 4ch
    xor al, al
    int 21h
  stop:
    mov ah,4ch
    int 21h


main endp
end main
